#ifndef CONFIG_H
#define CONFIG_H
#include <Arduino.h>

#define MAX_NAME_LENGTH 10
extern char* module_type;

String getConfigValue(String name);
String setConfigValue(String name, String value);

#endif
