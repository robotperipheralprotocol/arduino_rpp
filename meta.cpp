#include "meta.h"
#include "config.h"
#include "serial_rpp.h"
#include <stdint.h>

#define NAME_SEARCH_STRING F("module_name")

rppResponse getName(String body){
    String name = getConfigValue(NAME_SEARCH_STRING);
    name = name.substring(0, name.indexOf('\0'));
    rppResponse response = {RESPONSE_OK, name};
    return response;
}

rppResponse getType(String body){
    rppResponse response = {RESPONSE_OK, module_type};
    return response;
}

rppResponse setName(String body){
    rppResponse response = {RESPONSE_OK, body};
    if (body.length() < MAX_NAME_LENGTH){
        setConfigValue(NAME_SEARCH_STRING, body);
    } else {
        response.type = RESPONSE_INSUFFICIENT_STORAGE;
        response.message = "";
    }
    return response;
}

void initMeta(void){
    registerSerialResource("/meta/name", &getName, &setName);
    registerSerialResource("/meta/type", &getType, &notAllowedHandler);
}
