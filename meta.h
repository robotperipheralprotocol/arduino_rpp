/* This module provides information about the board it is connected
 * to. The module name can be changed, but the version number and
 * module type cannot
 *
 * It expects somewhere:
 *     #define MODULE_TYPE "power"
 *     #define SOFTWARE_VERSION "v10"
 * Or similar */

#ifndef META_H
#define META_H

void initMeta(void);

#endif
