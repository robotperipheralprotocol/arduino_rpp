const char* module_type = "test_module";

#include "serial_rpp.h"
#include "meta.h"

void setup() {
  initSerialRpp(115200);
  initMeta();
  sendAsyncMessage({ASYNC_DATA_AVAILABLE, "/", "Booted"});
}

void loop() {
  while(1){
        updateSerialRpp();
    }
}

