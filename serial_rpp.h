#ifndef SERIAL_HTTP_H
#define SERIAL_HTTP_H
#include <Arduino.h>

#define SERIAL_MAX_DIRECTORY_NODES 20
#define INPUT_BUFFER_LENGTH 100

typedef enum {
    RESPONSE_PROCESSING            = 102,
    RESPONSE_OK                    = 200,
    RESPONSE_BAD_REQUEST           = 400,
    RESPONSE_NOT_FOUND             = 404,
    RESPONSE_NOT_ALLOWED           = 405,
    RESPONSE_NOT_ACCEPTABLE        = 406,
    RESPONSE_REQUEST_TIMEOUT       = 408,
    RESPONSE_PRECONDITION_FAILED   = 412,
    RESPONSE_PAYLOAD_TOO_LARGE     = 413,
    RESPONSE_URI_TOO_LONG          = 414,
    RESPONSE_RANGE_NOT_SATISFIABLE = 416,
    RESPONSE_IM_A_TEAPOT           = 418,
    RESPONSE_LOCKED                = 423,
    RESPONSE_TOO_MANY_REQUESTS     = 429,
    RESPONSE_SERVER_ERROR          = 500,
    RESPONSE_NOT_IMPLEMENTED       = 501,
    RESPONSE_INSUFFICIENT_STORAGE  = 507,
} rppResponseCode;

typedef enum {
    ASYNC_DATA_AVAILABLE           = 600,
    ASYNC_INFORMATION              = 610,
    ASYNC_DEBUG                    = 611,
    ASYNC_WARNING                  = 612,
    ASYNC_ERROR                    = 613,
    ASYNC_DANGER_TO_TASK           = 620,
    ASYNC_DANGER_TO_PERIPHERAL     = 630,
    ASYNC_DANGER_TO_EXTERNAL       = 640,
    ASYNC_DANGER_TO_HUMAN          = 650,
    ASYNC_DANGER_TO_HUMAN_LIFE     = 655,
    ASYNC_APOCOLYPSE               = 660,
} rppAsyncCode;


#define VALUE_ERROR F("Invalid Value")

#define GET F("GET")
#define PUT F("PUT")

#define TRUE F("True")
#define FALSE F("False")

typedef struct {
    rppResponseCode type;
    String message;
} rppResponse;

typedef struct {
    rppAsyncCode type;
    String resource;
    String message;
} rppAsync;

typedef rppResponse (handler_function)(String);

rppResponse notAllowedHandler(String body);
rppResponse notImplementedHandler(String body);
void initSerialRpp(int baud);
void updateSerialRpp(void);
int registerSerialResource(String name, handler_function get, handler_function put);

void sendAsyncMessage(rppAsync message);

#endif
