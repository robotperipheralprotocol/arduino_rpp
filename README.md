## Intro
Arduino's are great. They're cheap and have a fair bit of hardware.
This is a 'bare' application with nothing except the required 'meta' folder
implemented. It supports changing the modules name, though not it's type

## Dependencies:
* 11kb Program space
* 507 bytes of dynamic memory
* The EEPROM free (it's managed by the config module)

## Basic Use:

1. Upload the sketch to an arduino.
2. Open rpp-gui
3. Give your arduino a name (type it in the PUT field)

## Including in your own program

You'll notice that the main arduino file is simple enough:
```
const char* module_type = "test_module";

#include "serial_rpp.h"
#include "meta.h"

void setup() {
  initSerialRpp(115200);
  initMeta();
  sendAsyncMessage({ASYNC_DATA_AVAILABLE, "/", "Booted"});
}

void loop() {
  while(1){
        updateSerialRpp();
    }
}
```
The module type must be defined before #include "serial_rpp.h"

To add your own URI's you use RegisterSerialResource. Here's an example use to 
toggle a pin (that has a laser on it):
```
const char* module_type = "test_module";

#include "serial_rpp.h"
#include "meta.h"

#define LASER_PIN 3

rppResponse getLaserEnabled(String body){
    rppResponse response = {RESPONSE_OK, ""};
    if (digitalRead(LASER_PIN) == HIGH){
        response.message += TRUE;  //True is #defined in serial_rpp as "True"
    } else {
        response.message += FALSE;
    }
    return response;
}

rppResponse putLaserEnabled(String body){
    rppResponse response = {RESPONSE_OK, ""};
    if (body == TRUE){
        digitalWrite(LASER_PIN, HIGH);
        return getLaserEnabled("");
    } else if (body == FALSE) {
        digitalWrite(LASER_PIN, LOW);
        return getLaserEnabled("");
    }
    response.message = VALUE_ERROR;
    return response;
}

void setup() {
  initSerialRpp(115200);
  initMeta();
  registerSerialResource("/laser/enabled", &getLaserEnabled, &putLaserEnabled);
  sendAsyncMessage({ASYNC_DATA_AVAILABLE, "/", "Booted"});
}

void loop() {
  while(1){
        updateSerialRpp();
    }
}
```

### Why not a library?
Because I don't like Arduino's library system. Instead, in modern versions of
arduino, you can use subfolders (which I prefer). So you can do:
```
#include arduino_rpp/serial_rpp.h
```
And then have the arduino_rpp folder a git submodule of this repository!