#include "config.h"
#include "serial_rpp.h"
#include <Arduino.h>
#include <EEPROM.h>

#define NUM_CONFIG_RESOURCES 1

typedef struct{
    String name;
    uint8_t length;
    uint8_t writable;
} configParameter_t;

configParameter_t CONFIG_RESOURCES[10] = {
    {"module_name", MAX_NAME_LENGTH},
};

String getConfigValue(String name){
    uint16_t offset = 0;
    String outStr = "";
    for (int i=0; i<NUM_CONFIG_RESOURCES; i++){
        if (name == CONFIG_RESOURCES[i].name){
            for (int j=0; j<CONFIG_RESOURCES[i].length; j++){
                outStr += (char)(EEPROM.read(offset+j));
            }
            return outStr;
        } else {
            offset += CONFIG_RESOURCES[i].length;
        }
    }
    return "\0";
}

String setConfigValue(String name, String value){
    uint16_t offset = 0;
    String outStr = "";
    for (int i=0; i<NUM_CONFIG_RESOURCES; i++){
        if (name == CONFIG_RESOURCES[i].name){
            for (int j=0; j<CONFIG_RESOURCES[i].length; j++){
                if (j <= value.length()){
                    EEPROM.write(offset+j, value[j]);
                } else {
                    EEPROM.write(offset+j, '\0');
                }
            }
            return outStr;
        } else {
            offset += CONFIG_RESOURCES[i].length;
        }
    }
    return outStr;
}
