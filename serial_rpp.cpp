#include <Arduino.h>

#include "serial_rpp.h"

#define END_OF_MESSAGE_CHAR '\x03' //EOF
#define MESSAGE_BODY_SEPERATOR '\x02'
#define MESSAGE_BODY_SEPERATOR_LENGTH 1

typedef struct{
    String name;
    handler_function* get;
    handler_function* put;
} serial_handler_t;


int numKnownSerialHandlers = 0;
serial_handler_t knownSerialHandlers[SERIAL_MAX_DIRECTORY_NODES];
String inputBuffer;

//-------------------------- Private Functions --------------------------------


static void outputResponse(rppResponse response, String resource){
    //Outputs a message in the correct format
    Serial.print(response.type);
    Serial.print(" ");
    Serial.print(resource);
    Serial.print(MESSAGE_BODY_SEPERATOR);
    Serial.print(response.message);
    Serial.print(END_OF_MESSAGE_CHAR);
}


static void handleRequest(String requestType, String resource, String body){
    //Sends a request off to the specified resource handler
    rppResponse response {RESPONSE_NOT_FOUND, ""};

    if (resource.endsWith("/")){
        if (requestType == GET){
            for (int i=0; i<numKnownSerialHandlers; i++){                      //For each known handler
                if (knownSerialHandlers[i].name.startsWith(resource)){         //If in the directory
                    String tmp_name = knownSerialHandlers[i].name;             //Get it's name
                    int seperator = tmp_name.indexOf('/', resource.length());  //Remove any sub-resources
                    if (seperator != -1){
                        tmp_name = tmp_name.substring(0, seperator) + "/";
                    }
                    if (response.message.indexOf(tmp_name) == -1){             //If it's not already in the response
                        response.message += tmp_name;                          //Add it to the response
                        response.message += '\n';
                    }
                    response.type = RESPONSE_OK;
                }
            }

        } else {
            response.type = RESPONSE_NOT_ALLOWED;
        }
    } else {
        for (int i=0; i<numKnownSerialHandlers; i++){                           //For each known handler
            if (resource == knownSerialHandlers[i].name){                       //See if it's the one we're looking for
                if (requestType == GET){
                    response = knownSerialHandlers[i].get(body);                //Either do a get
                } else if (requestType == PUT){
                    response = knownSerialHandlers[i].put(body);                //Or do a put
                } else {
                    response.type = RESPONSE_NOT_IMPLEMENTED;                   //or say it's not implemented
                }
            }
        }
    }
    outputResponse(response, resource);
}

static void parseInput(String raw){
    //Parses a document
    rppResponse output = {RESPONSE_OK, ""};
    int headerSeperatorIndex = raw.indexOf(MESSAGE_BODY_SEPERATOR);
    if (headerSeperatorIndex != -1){                                           //Checks the document can be divided into a header and a body
        String body = raw.substring(headerSeperatorIndex + \
            MESSAGE_BODY_SEPERATOR_LENGTH);
        String header = raw.substring(0, headerSeperatorIndex);
        int requestTypeSeperator = header.indexOf(' ');
        if (requestTypeSeperator == -1){                                       //Check the header consists of two parts
            output.type = RESPONSE_BAD_REQUEST;
        } else {
            String requestType = header.substring(0, requestTypeSeperator);
            String resource = header.substring(requestTypeSeperator + 1);
            resource.trim();
            requestType.trim();
            handleRequest(requestType, resource, body);
            return;
        }
    } else {
        output.type = RESPONSE_BAD_REQUEST;
    }
    outputResponse(output, "/");
}

void updateSerialRpp(void){
    //Checks serial buffer and if the end of transmission character is
    //received, starts parsing it
    while (Serial.available() > 0){                                            //Append any new characters from serial
        inputBuffer += (char)(Serial.read());
        int terminator_pos = inputBuffer.indexOf(END_OF_MESSAGE_CHAR);
        if (terminator_pos != -1){
            inputBuffer = inputBuffer.substring(0, inputBuffer.length() - 1);
            parseInput(inputBuffer);
            inputBuffer = "";
            break;
        }
    }
}

//-------------------------- Public Functions --------------------------------
void initSerialRpp(int baud){
    //Initializes the HTTP server and registers the root directory and
    //a test echo handler
    Serial.begin(baud);
    inputBuffer.reserve(INPUT_BUFFER_LENGTH);
}

int registerSerialResource(String name, handler_function get, handler_function put){
    //Adds a file/directory to the server
    if (numKnownSerialHandlers >= SERIAL_MAX_DIRECTORY_NODES){                                    //Check there is room in the directory tree
        return 0; //No room in buffer
    }
    knownSerialHandlers[numKnownSerialHandlers].name = name;
    knownSerialHandlers[numKnownSerialHandlers].get = get;
    knownSerialHandlers[numKnownSerialHandlers].put = put;
    numKnownSerialHandlers += 1;
    return 1;
}

void sendAsyncMessage(rppAsync message){
    rppResponse fakeResponse;
    fakeResponse.message = message.message;
    fakeResponse.type = (rppResponseCode)message.type;
    outputResponse(fakeResponse, message.resource);
}


//------------------------- Default Handlers ----------------------------------

rppResponse notImplementedHandler(String body){
    rppResponse response;                                                               //Can't put documents in the tree root because this would be too hard to implement
    response.type = RESPONSE_NOT_IMPLEMENTED;
    response.message = "";
    return response;
}
rppResponse notAllowedHandler(String body){
    rppResponse response;                                                               //Can't put documents in the tree root because this would be too hard to implement
    response.type = RESPONSE_NOT_ALLOWED;
    response.message = "";
    return response;
}
